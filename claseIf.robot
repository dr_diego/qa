*** Settings ***
Library				SeleniumLibrary

*** Variables ***

${palabraABuscar}	Diego Betancourt
${homePage}		http://automationpractice.com/index.php
${seleccion}	Other

*** Keywords ***
Select Women Option
	Click Element	xpath=//*[@id="block_top_menu"]/ul/li[1]/a
	Title Should Be		Women - My Store

Select Dresses Option
	Click Element	xpath=//*[@id="block_top_menu"]/ul/li[2]/a
	Title Should Be		Dresses - My Store
	
*** Test Cases ***
G001 Caso con Condicionales
	Open Browser		${homePage}		chrome
	Wait Until Element Is Visible	xpath=//*[@id="header_logo"]/a/img
	Run Keyword If	'${seleccion}'=='Women'		Select Women Option		ELSE Select Dresses Option
	Close Browser