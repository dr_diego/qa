*** Settings ***
Library		SeleniumLibrary
Library		String

*** Variables ***

${homePage}		automationpractice.com/index.php
${scheme}		http
${textURL}		${scheme}://${homePage}

*** Keywords ***
Open Homepage
	Open Browser		${textURL}		chrome
	
*** Test Cases ***
C001 Hacer Click en Contenedores
	Open Homepage
	Set Global Variable		@{nombresDeContenedores}	//*[@id="homefeatured"]/li[1]/div/div[2]/h5/a	//*[@id="homefeatured"]/li[2]/div/div[2]/h5/a	//*[@id="homefeatured"]/li[3]/div/div[2]/h5/a	//*[@id="homefeatured"]/li[4]/div/div[2]/h5/a	//*[@id="homefeatured"]/li[5]/div/div[2]/h5/a	//*[@id="homefeatured"]/li[6]/div/div[2]/h5/a	//*[@id="homefeatured"]/li[7]/div/div[2]/h5/a
	FOR	${nombreDeContenedor}	IN	@{nombresDeContenedores}
		RUN Keyword if	'${nombreDeContenedor}'=='//*[@id="homefeatured"]/li[7]/div/div[2]/h5/a'	Exit For Loop
		Click Element	xpath=${nombreDeContenedor}
		Wait Until Element Is Visible	xpath=//*[@id="bigpic"]
		Click Element	//*[@id="header_logo"]/a/img
	END
	Close Browser

C002 Caso de Prueba Nueva
	Open Homepage