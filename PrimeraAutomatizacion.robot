*** Settings ***
Resource	Recursos.robot

*** Test Cases ***
G001 Busqueda de palabras en Google
	Abrir Navegador y Esperar Logo
	Input Text		xpath=/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input	${palabraABuscar}
	Click Element	xpath=/html/body/div[1]/div[2]
	Click Element 	xpath=/html/body/div[1]/div[3]/form/div[1]/div[1]/div[3]/center/input[1]	
	Title Should Be		${palabraABuscar} - Buscar con Google
	Page Should Contain		${palabraABuscar}
	Close Browser
	
G002 Hacer click en el boton de busqueda sin escribir palabras en Google
	Abrir Navegador y Esperar Logo
	Click Element 	xpath=/html/body/div[1]/div[3]/form/div[1]/div[1]/div[3]/center/input[1]
	Title Should Be		Google	
	Close Browser	
	
